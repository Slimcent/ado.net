﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirstModelDAL.Models;


namespace CodeFirstModelDAL.EF
{
    public class MyDataInitializer : DropCreateDatabaseAlways<BEZAOPayEntities>
    {
        protected override void Seed(BEZAOPayEntities context)
        {
            base.Seed(context);
            var AddUser = new List<User>
            {
                new User {Name = "Dave", Email = "dave@gmail.com"},
                new User {Name = "Matt", Email = "matt@domain.com"},
                new User {Name = "Steve", Email = "ste@domain.com"},
                new User {Name = "Pat", Email = "pat@domain.com"},
            };
            context.Users.AddOrUpdate(u => new { u.Name, u.Email }, AddUser.ToArray());
        }
    }
}
