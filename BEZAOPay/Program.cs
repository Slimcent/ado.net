﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEZAOPayDAL;
using System.Configuration;
using System.Data.SqlClient;


namespace BEZAOPay
{
    class Program
    {
        static void Main(string[] args)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["BezaoConnect"].ConnectionString;
            var db = new BEZAODAL();

            //var users = db.GetAllUsers();
            //foreach (var user in users)
            //{
                //Console.WriteLine($"Id: {user.Id}\nName: {user.Name}\nEmail: {user.Email}");
           // }

            // Adding a new user
            //db.InsertUser(new BEZAOPayDAL.Models.User { Name = "Ake", Email = "Ak@gmail.com" });
            //Console.WriteLine("Name added successfully.\n");
            //foreach (var user in db.GetAllUsers())
            //{
                //Console.WriteLine($"Id: {user.Id}\nName: {user.Name}\nEmail: {user.Email}");
            //}

            // Sending Money using Transaction technigue
            //db.SendMoney(new BEZAOPayDAL.Models.Account {UserId = 1004, Balance = 5000.99m },
                         //new BEZAOPayDAL.Models.Transaction { UserId = 1004, Mode = "Credit", Amount = 5000.99m, Time = DateTime.Now });
            

            // Adding a new Account
            /*db.InsertAccount(new BEZAOPayDAL.Models.Account { UserId = 1004, AccountNumber = 1000050000, Balance = 50000.99m });
            Console.WriteLine("Name added successfully.\n");
            */

            // Update a user
            //db.UpdateUser(new BEZAOPayDAL.Models.User { Id = 17, Name = "Emmanuel" });

            // Deleting a user
            try
            {
                db.DeleteUser(1009);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Something went wrong: {ex.Message}");
            }
            Console.ReadKey();
        }
    }
}
