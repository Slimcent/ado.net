﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using BEZAOPayDAL.Models;

namespace BEZAOPayDAL
{
    public class BEZAODAL
    {
        private readonly string _connectionString;

        public BEZAODAL() :
            this(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=BEZAOPay;Integrated Security=True")
        {

        }

        public BEZAODAL(string connectionString)
        {
            _connectionString = connectionString;
        }


        private SqlConnection _sqlConnection = null;
        private void OpenConnection()
        {
            _sqlConnection = new SqlConnection { ConnectionString = _connectionString };
            _sqlConnection.Open();
        }

        private void CloseConnection()
        {
            if (_sqlConnection?.State != ConnectionState.Closed)
                _sqlConnection?.Close();
        }


        public IEnumerable<User> GetAllUsers()
        {
            OpenConnection();

            var users = new List<User>();

            var query = @"SELECT * FROM USERS";

            using (var command = new SqlCommand(query, _sqlConnection))
            {
                command.CommandType = CommandType.Text;
                var reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                while (reader.Read())
                {
                    users.Add(new User
                    {
                        Id = (int)reader["Id"],
                        Name = (string)reader["Name"],
                        Email = (string)reader["Email"]


                    });
                }
                reader.Close();
            }
            return users;
        }

        public void InsertNewUser(User user)
        {
            try
            {
                OpenConnection();
                string query = "Insert Into Users (Name, Email) Values " +
                $"('{user.Name}', '{user.Email}')";
                using (SqlCommand command = new SqlCommand(query, _sqlConnection))
                {
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }



        public void DeleteUser(int id)
        {
            try
            {
                OpenConnection();
                string sql = $"Delete from Users where Id = '{id}'";
                using (SqlCommand command = new SqlCommand(sql, _sqlConnection))
                {
                    try
                    {
                        command.CommandType = CommandType.Text;
                        int deletedUser = command.ExecuteNonQuery();
                        if (deletedUser > 0)
                        {
                            Console.WriteLine($"User deleted successully");
                        }
                        else
                        {
                            Console.WriteLine("Delete failed");
                            
                        }
                    }
                    catch (SqlException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            CloseConnection();
        }

        public void UpdateUser(User user)
        {
            OpenConnection();
            string sql = $"Update Users Set Name = '{user.Name}' Where Id = '{user.Id}'";
            using (SqlCommand command = new SqlCommand(sql, _sqlConnection))
            {
                try
                {
                   int comm = command.ExecuteNonQuery();
                    if (comm > 0)
                    {
                        Console.WriteLine("Update Successful");
                        Console.WriteLine($"Name updated to {user.Name}");
                    }
                    else
                    {
                        Console.WriteLine("Update Failed! Id not found");
                    }               
                }
                catch (SqlException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            CloseConnection();
        }

       
        public void InsertUser(User user)
        {
            try
            {
                OpenConnection();
                using (SqlCommand command = new SqlCommand("InsertIntoUsers", _sqlConnection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter parameter = new SqlParameter
                    {
                        ParameterName = "@name",
                        Value = user.Name,
                        SqlDbType = SqlDbType.VarChar,
                        Size = 50,
                        Direction = ParameterDirection.Input
                    };
                    command.Parameters.Add(parameter);

                    parameter = new SqlParameter
                    {
                        ParameterName = "@email",
                        Value = user.Email,
                        SqlDbType = SqlDbType.VarChar,
                        Size = 50,
                        Direction = ParameterDirection.Input
                    };
                    command.Parameters.Add(parameter);
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public void InsertAccount(Account account)
        {
            try
            {
                OpenConnection();
                using (SqlCommand command = new SqlCommand("InsertIntoAccounts", _sqlConnection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter parameter = new SqlParameter
                    {
                        ParameterName = "@userID",
                        Value = account.UserId,
                        SqlDbType = SqlDbType.Int,
                        Size = 50,
                        Direction = ParameterDirection.Input
                    };
                    command.Parameters.Add(parameter);

                    parameter = new SqlParameter
                    {
                        ParameterName = "@accountNumber",
                        Value = account.AccountNumber,
                        SqlDbType = SqlDbType.Int,
                        Size = 50,
                        Direction = ParameterDirection.Input
                    };
                    command.Parameters.Add(parameter);

                    parameter = new SqlParameter
                    {
                        ParameterName = "@balance",
                        Value = account.Balance,
                        SqlDbType = SqlDbType.Decimal,
                        Size = 50,
                        Direction = ParameterDirection.Input
                    };
                    command.Parameters.Add(parameter);

                    command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public void SendMoney(Account account, Transaction transact)
        {
            OpenConnection();
            string sql = $"Update Accounts Set Balance = '{account.Balance += account.Balance}' Where UserId = '{account.UserId}'";
            string query = "Insert Into Transactions (UserId, Mode, Amount, Time) Values " +
                $"('{transact.UserId}', '{transact.Mode}', '{transact.Amount}', '{transact.Time}')";

            var cmdSql = new SqlCommand(sql, _sqlConnection);
            var cmdQuery = new SqlCommand(query, _sqlConnection);

            SqlTransaction tx = null;

            try
            {
                tx = _sqlConnection.BeginTransaction();
                cmdSql.Transaction = tx;
                cmdQuery.Transaction = tx;

                cmdSql.ExecuteNonQuery();
                cmdQuery.ExecuteNonQuery();

                tx.Commit();
                Console.WriteLine("Transaction was successful");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                tx?.Rollback();
            }
            finally
            {
                CloseConnection();
            }
        }


    }
}
