﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodeFirstModelDAL.Models;
using CodeFirstModelDAL.EF;
using System.Data.Entity;

namespace CodeFirstModelConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            //Database.SetInitializer(new MyDataInitializer());
            Console.WriteLine("***** Fun with ADO.NET EF Code First *****\n");
            using (var context = new BEZAOPayEntities())
            {
                foreach (User u in context.Users)
                {
                    Console.WriteLine(u.Name, u.Email);
                }
            }
            Console.ReadLine();
        }
    }
}
