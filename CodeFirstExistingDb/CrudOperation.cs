﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using static System.Console;
using CodeFirstExistingDb.Ef;
using BEZAOPayDAL.Models;
using BEZAOPayDAL;
using System.Data.Entity;

namespace CodeFirstExistingDb
{
    public static class CrudOperation
    {
        public static void UpdateUser(int id)
        {
            using (var context = new BEZAOPayEntities())
            {
                Ef.User userToUpdate = context.Users.Find(id);
                if (userToUpdate != null)
                {
                    WriteLine(context.Entry(userToUpdate).State);
                    userToUpdate.Name = "Check";
                    WriteLine(context.Entry(userToUpdate).State);
                    context.SaveChanges();
                }
            }
        }

        public static int AddNewUser()
        {
            using (var context = new BEZAOPayEntities())
            {
                try
                {
                    var user = new Ef.User() { Name = "Entity", Email = "e@gmail.com" };
                    context.Users.Add(user);
                    context.SaveChanges();
                    return user.Id;
                }
                catch (Exception ex)
                {
                    WriteLine(ex.InnerException?.Message);
                    return 0;
                }
            }
        }

        public static void ShowAllUsers()
        {
            using (var context = new BEZAOPayEntities())
            {
                foreach (Ef.User user in context.Users)
                {
                    WriteLine(user.Id.ToString() + " " + user.Name + " " + user.Email);
                }
            }
        }

        public static void FindUser(int id)
        {
            using (var context = new BEZAOPayEntities())
            {
                Ef.User userToFind = context.Users.Find(id);
                if (userToFind != null)
                {
                    Console.WriteLine(userToFind.Id.ToString() + " " + userToFind.Name + " " + userToFind.Email);
                }
                else
                {
                    Console.WriteLine("User not found");
                }
            }
        }

        public static void DeleteUser(int id)
        {
            using (var context = new BEZAOPayEntities())
            {
                Ef.User userToDelete = context.Users.Find(id);
                if (userToDelete != null)
                {
                    context.Users.Remove(userToDelete);
                    Console.WriteLine("User deleted");
                    if (context.Entry(userToDelete).State != EntityState.Deleted)
                    {
                        throw new Exception("Unable to delete the record");
                    }
                    context.SaveChanges();
                }
                else
                {
                    Console.WriteLine("User not found");
                }
            }
        }
    }
}
